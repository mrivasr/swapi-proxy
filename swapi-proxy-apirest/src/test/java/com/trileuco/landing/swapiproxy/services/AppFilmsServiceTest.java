package com.trileuco.landing.swapiproxy.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.trileuco.landing.swapiproxy.data.dataservices.FilmsService;
import com.trileuco.landing.swapiproxy.data.dtos.SwapiFilmDTO;
import com.trileuco.landing.swapiproxy.dtos.AppFilmDTO;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class AppFilmsServiceTest {

    @Mock
    private FilmsService filmRepo;

    @Autowired
    @InjectMocks
    private SWapiServiceImpl swapiService;

    @Test
    public void testGetFilmsFromUrl() {

        LocalDate release = LocalDate.of(1980, 1, 1);
        String title = "Star Wars 1";
        List<String> urls = new ArrayList<String>();
        urls.add("https://urlExample");

        SwapiFilmDTO starWars = new SwapiFilmDTO(null, null, "director", null, 1, "opening crawl", null, "producer",
                release, null, null, title, "url", null);
        List<SwapiFilmDTO> expected = new ArrayList<SwapiFilmDTO>();
        expected.add(starWars);

        when(filmRepo.getSwapiFilmsFromUrlLists(urls)).thenReturn(expected);

        List<AppFilmDTO> result = swapiService.getAppFilmsFromUrlList(urls);

        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0).getName()).isEqualTo(title);
        assertThat(result.get(0).getReleaseDate()).isEqualTo(release);
    }
}
