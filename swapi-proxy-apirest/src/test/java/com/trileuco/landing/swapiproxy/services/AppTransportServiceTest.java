package com.trileuco.landing.swapiproxy.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.trileuco.landing.swapiproxy.data.dataservices.StarshipsService;
import com.trileuco.landing.swapiproxy.data.dataservices.VehiclesService;
import com.trileuco.landing.swapiproxy.data.dtos.SwapiStarshipDTO;
import com.trileuco.landing.swapiproxy.data.dtos.SwapiVehicleDTO;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class AppTransportServiceTest {

    @Mock
    private StarshipsService starshipsRepo;

    @Mock
    private VehiclesService vehiclesRepo;

    @Autowired
    @InjectMocks
    private SWapiServiceImpl swapiService;

    @Test
    public void testGetNameOfastestTransport() {

        String lowSpeed = "10";
        String lowSpeedName = "low vehicle";
        String highSpeed = "100";
        String highSpeedName = "high starship";

        List<String> urlVehicles = new ArrayList<String>();
        urlVehicles.add("https://urlExample1");

        List<String> urlStarships = new ArrayList<String>();
        urlStarships.add("https://urlExample2");

        SwapiVehicleDTO swVehicle = new SwapiVehicleDTO("cargo", "consumables", "cost", LocalDateTime.now(), "crew",
                LocalDateTime.now(), "length", "manufacturer", lowSpeed, "model", lowSpeedName, "passangers", null,
                null, "url", "class");

        List<SwapiVehicleDTO> vehicles = new ArrayList<SwapiVehicleDTO>();
        vehicles.add(swVehicle);

        SwapiStarshipDTO swStarship = new SwapiStarshipDTO("mglt", "cargo", "consumables", "cost", LocalDateTime.now(),
                "crew", LocalDateTime.now(), "hprating", "length", "manufacturer", highSpeed, "model", highSpeedName,
                "passangers", null, null, "class", "url");

        List<SwapiStarshipDTO> starships = new ArrayList<SwapiStarshipDTO>();
        starships.add(swStarship);

        when(vehiclesRepo.getSwapiVehiclesFromUrlList(urlVehicles)).thenReturn(vehicles);

        when(starshipsRepo.getSwapiStarshipsFromUrlList(urlStarships)).thenReturn(starships);

        String result = swapiService.getNameOfFastestTransport(urlVehicles, urlStarships);

        assertThat(result).isEqualTo(highSpeedName);
    }

    @Test
    public void testGetNameOfTransportNa() {

        String nAspeed = "n/a";
        String expectedName = "none";

        List<String> urlVehicles = new ArrayList<String>();
        urlVehicles.add("https://urlExample1");

        List<String> urlStarships = new ArrayList<String>();
        urlStarships.add("https://urlExample2");

        SwapiVehicleDTO swVehicle = new SwapiVehicleDTO("cargo", "consumables", "cost", LocalDateTime.now(), "crew",
                LocalDateTime.now(), "length", "manufacturer", nAspeed, "model", "xwing", "passangers", null, null,
                "url", "class");

        List<SwapiVehicleDTO> vehicles = new ArrayList<SwapiVehicleDTO>();
        vehicles.add(swVehicle);

        SwapiStarshipDTO swStarship = new SwapiStarshipDTO("mglt", "cargo", "consumables", "cost", LocalDateTime.now(),
                "crew", LocalDateTime.now(), "hprating", "length", "manufacturer", nAspeed, "model", "ywing",
                "passangers", null, null, "class", "url");

        List<SwapiStarshipDTO> starships = new ArrayList<SwapiStarshipDTO>();
        starships.add(swStarship);

        when(vehiclesRepo.getSwapiVehiclesFromUrlList(urlVehicles)).thenReturn(vehicles);

        when(starshipsRepo.getSwapiStarshipsFromUrlList(urlStarships)).thenReturn(starships);

        String result = swapiService.getNameOfFastestTransport(urlVehicles, urlStarships);

        assertThat(result).isEqualTo(expectedName);

    }

}
