package com.trileuco.landing.swapiproxy.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.trileuco.landing.swapiproxy.dtos.AppPeopleDTO;
import com.trileuco.landing.swapiproxy.exceptions.SearchNotFoundException;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class SWapiServiceTest {

    @Autowired
    private SWapiService swapiService;

    @Value("${project.swapi.baseurl}")
    private String BASE_URL;

    @Value("${project.swapi.searchendpoint}")
    private String EXTERNAL_ENDPOINT;

    @Test
    public void testGetCharacteresByName() throws SearchNotFoundException {
        String testUrl = BASE_URL + EXTERNAL_ENDPOINT;
        String name = "Luke";
        String expectedName = "Luke Skywalker";
        String expectedPlanet = "Tatooine";

        List<AppPeopleDTO> result = swapiService.getCharacteresByName(testUrl, name);
        assertThat(result.size()).isEqualTo(1);

        AppPeopleDTO resultedPerson = result.get(0);

        assertThat(resultedPerson.getName()).isEqualTo(expectedName);
        assertThat(resultedPerson.getPlanetName()).isEqualTo(expectedPlanet);
    }

}
