package com.trileuco.landing.swapiproxy.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.trileuco.landing.swapiproxy.dtos.AppFilmDTO;
import com.trileuco.landing.swapiproxy.dtos.AppPeopleDTO;
import com.trileuco.landing.swapiproxy.exceptions.SearchNotFoundException;
import com.trileuco.landing.swapiproxy.services.SWapiService;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class SwapiProxyControllerTest {

    @Mock
    private SWapiService swapiService;

    @InjectMocks
    private SwapiProxyController swapiProxyController;

    @Test
    public void testGetPersonInfo() throws SearchNotFoundException {

        String nameToSearch = "luke";

        List<AppFilmDTO> films = new ArrayList<AppFilmDTO>();
        List<AppPeopleDTO> expected = new ArrayList<AppPeopleDTO>();

        AppFilmDTO empireStrikes = new AppFilmDTO("The empire strikes back", LocalDate.of(1980, 2, 2));
        AppPeopleDTO luke = new AppPeopleDTO("Luke", "18BBY", "male", "tatooine", "x-wing", films);

        films.add(empireStrikes);
        expected.add(luke);

        when(swapiService.getCharacteresByName(any(String.class), eq(nameToSearch))).thenReturn(expected);
        List<AppPeopleDTO> result = swapiProxyController.getPersonInfo(nameToSearch);

        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0).getName()).isEqualTo("Luke");
        assertThat(result.get(0).getPlanetName()).isEqualTo("tatooine");

    }

    @Test
    public void testSearchNotFoundException() throws SearchNotFoundException {
        String nameToSearch = "luke";

        when(swapiService.getCharacteresByName(any(String.class), eq(nameToSearch)))
                .thenThrow(SearchNotFoundException.class);

        Assertions.assertThrows(SearchNotFoundException.class, () -> {
            swapiProxyController.getPersonInfo(nameToSearch);
        });
    }

}
