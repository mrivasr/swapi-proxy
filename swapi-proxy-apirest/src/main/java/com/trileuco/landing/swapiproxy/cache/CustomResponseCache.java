package com.trileuco.landing.swapiproxy.cache;

import org.apache.commons.collections4.map.LRUMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CustomResponseCache<K, T> {

    private LRUMap<K, CacheObject> responseCacheMap;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    protected class CacheObject {
        public long lastAccessed = System.currentTimeMillis();
        public T value;

        protected CacheObject(T value) {
            this.value = value;
        }
    }

    @Autowired
    public CustomResponseCache(@Value("${project.swapi.cache.maxitems}") int maxItems,
            @Value("${project.swapi.cache.timerinterval}") long timerInterval) {

        responseCacheMap = new LRUMap<K, CacheObject>(maxItems);

        Thread t = new Thread(new Runnable() {
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(timerInterval * 1000);
                    } catch (InterruptedException ex) {
                    }
                }
            }
        });

        t.setDaemon(true);
        t.start();
    }

    public void put(K key, T value) {
        synchronized (responseCacheMap) {
            responseCacheMap.put(key, new CacheObject(value));
        }
    }

    public T get(K key) {
        synchronized (responseCacheMap) {
            CacheObject cacheResult = (CacheObject) responseCacheMap.get(key);

            if (cacheResult == null) {
                log.info("Result for {} not present in cache", key.toString());
                return null;
            } else {
                cacheResult.lastAccessed = System.currentTimeMillis();
                log.info("Returned cached value for {}", key.toString());
                return cacheResult.value;
            }
        }
    }

    public void remove(K key) {
        synchronized (responseCacheMap) {
            responseCacheMap.remove(key);
        }
    }
}
