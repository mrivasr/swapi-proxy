package com.trileuco.landing.swapiproxy.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.trileuco.landing.swapiproxy.dtos.AppPeopleDTO;
import com.trileuco.landing.swapiproxy.dtos.ErrorDTO;
import com.trileuco.landing.swapiproxy.exceptions.SearchNotFoundException;
import com.trileuco.landing.swapiproxy.services.SWapiService;

@RestController
@RequestMapping("swapi-proxy")
public class SwapiProxyController {

    @Autowired
    private SWapiService swapiService;

    @Value("${project.swapi.baseurl}")
    private String BASE_URL;

    @Value("${project.swapi.searchendpoint}")
    private String EXTERNAL_ENDPOINT;

    @ExceptionHandler(SearchNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorDTO handleSearchNotFoundException(SearchNotFoundException exception) {
        return new ErrorDTO(exception.getErrorMessage());

    }

    @GetMapping("/person-info")
    public List<AppPeopleDTO> getPersonInfo(@RequestParam(defaultValue = "") String name)
            throws SearchNotFoundException {

        String searchUrl = BASE_URL + EXTERNAL_ENDPOINT;
        List<AppPeopleDTO> appPeople = swapiService.getCharacteresByName(searchUrl, name);

        return appPeople;
    }
}
