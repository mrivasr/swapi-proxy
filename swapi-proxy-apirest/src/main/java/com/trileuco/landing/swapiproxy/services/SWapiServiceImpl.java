package com.trileuco.landing.swapiproxy.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.trileuco.landing.swapiproxy.assembler.Assembler;
import com.trileuco.landing.swapiproxy.cache.CustomResponseCache;
import com.trileuco.landing.swapiproxy.data.dataservices.FilmsService;
import com.trileuco.landing.swapiproxy.data.dataservices.PlanetsService;
import com.trileuco.landing.swapiproxy.data.dataservices.SearchService;
import com.trileuco.landing.swapiproxy.data.dataservices.StarshipsService;
import com.trileuco.landing.swapiproxy.data.dataservices.VehiclesService;
import com.trileuco.landing.swapiproxy.data.dtos.SwapiFilmDTO;
import com.trileuco.landing.swapiproxy.data.dtos.SwapiPeopleDTO;
import com.trileuco.landing.swapiproxy.data.dtos.SwapiPlanetDTO;
import com.trileuco.landing.swapiproxy.data.dtos.SwapiSearchResultDTO;
import com.trileuco.landing.swapiproxy.data.dtos.SwapiStarshipDTO;
import com.trileuco.landing.swapiproxy.data.dtos.SwapiVehicleDTO;
import com.trileuco.landing.swapiproxy.dtos.AppFilmDTO;
import com.trileuco.landing.swapiproxy.dtos.AppPeopleDTO;
import com.trileuco.landing.swapiproxy.exceptions.SearchNotFoundException;
import com.trileuco.landing.swapiproxy.util.TransportType;

@Service
public class SWapiServiceImpl implements SWapiService {

    @Autowired
    private PlanetsService planetsService;

    @Autowired
    private SearchService searchService;

    @Autowired
    private FilmsService filmService;

    @Autowired
    private VehiclesService vehiclesService;

    @Autowired
    private StarshipsService starshipService;

    @Autowired
    private Assembler assembler;

    @Autowired
    private CustomResponseCache<String, List<AppPeopleDTO>> cache;

    @Value("${project.swapi.transports.nospeedvalue}")
    private Long NO_SPEED_VALUE;

    @Override
    public List<AppPeopleDTO> getCharacteresByName(String searchUrl, String name) throws SearchNotFoundException {

        List<AppPeopleDTO> cachedResult = cache.get(name);
        if (cachedResult != null) {
            return cachedResult;
        }

        SwapiSearchResultDTO queryResult = searchService.getCharacteresByKeywordsFromUrl(searchUrl + name);

        if (queryResult.getCount() == 0) {
            throw new SearchNotFoundException(name);
        }

        List<SwapiPeopleDTO> peopleDtos = queryResult.getResults();

        // Get results from all the pages returned from the API.
        if (queryResult.getNext() != null) {

            String tempNextSearch = queryResult.getNext();

            while (tempNextSearch != null) {
                SwapiSearchResultDTO tmpResult = searchService.getCharacteresByKeywordsFromUrl(tempNextSearch);
                peopleDtos.addAll(tmpResult.getResults());
                tempNextSearch = tmpResult.getNext();
            }
        }

        List<AppPeopleDTO> characteresResult = fromSwapiPeopleListToAppPeopleList(peopleDtos);
        cache.put(name, characteresResult);

        return characteresResult;
    }

    private List<AppPeopleDTO> fromSwapiPeopleListToAppPeopleList(List<SwapiPeopleDTO> swpeople) {
        return swpeople.stream().map(sp -> fromSwapiPeopleToAppPeopleDto(sp)).collect(Collectors.toList());
    }

    private AppPeopleDTO fromSwapiPeopleToAppPeopleDto(SwapiPeopleDTO swpeople) {

        String planetName = getPlanetName(swpeople);

        String fastestVehicleDriven = getNameOfFastestTransport(swpeople.getVehicles(), swpeople.getStarships());

        List<AppFilmDTO> films = getAppFilmsFromUrlList(swpeople.getFilms());

        return new AppPeopleDTO.Builder().name(swpeople.getName()).birthYear(swpeople.getBirthYear())
                .gender(swpeople.getGender()).planetName(planetName).fastestVehicleDriven(fastestVehicleDriven)
                .films(films).build();
    }

    private String getPlanetName(SwapiPeopleDTO swpeople) {
        SwapiPlanetDTO swapiPlanet = planetsService.getSwapiPlanetFromUrl(swpeople.getHomeworld());
        return swapiPlanet.getName();
    }

    protected List<AppFilmDTO> getAppFilmsFromUrlList(List<String> filmUrls) {

        List<SwapiFilmDTO> filmDtos = filmService.getSwapiFilmsFromUrlLists(filmUrls);
        return assembler.fromSwapiFilmListToAppFilmList(filmDtos);
    }

    protected String getNameOfFastestTransport(List<String> urlVehicles, List<String> urlStarships) {

        TransportType fastest = new TransportType("none", NO_SPEED_VALUE);

        List<SwapiVehicleDTO> sVehicleDtos = vehiclesService.getSwapiVehiclesFromUrlList(urlVehicles);
        List<SwapiStarshipDTO> sStarshDtos = starshipService.getSwapiStarshipsFromUrlList(urlStarships);

        List<TransportType> appTransports = assembler.convertToAppTransport(sVehicleDtos, sStarshDtos);

        for (TransportType transport : appTransports) {
            if (transport.getAtmospheringSpeed() > fastest.getAtmospheringSpeed()) {
                fastest = transport;
            }
        }
        return fastest.getName();
    }

}
