package com.trileuco.landing.swapiproxy.assembler;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.trileuco.landing.swapiproxy.data.dtos.SwapiFilmDTO;
import com.trileuco.landing.swapiproxy.data.dtos.SwapiStarshipDTO;
import com.trileuco.landing.swapiproxy.data.dtos.SwapiVehicleDTO;
import com.trileuco.landing.swapiproxy.dtos.AppFilmDTO;
import com.trileuco.landing.swapiproxy.util.TransportType;

@Component
public class Assembler {

    @Value("${project.swapi.transports.nospeedvalue}")
    private Long NO_SPEED_VALUE;

    private final static Logger log = LoggerFactory.getLogger(Assembler.class);

    public final List<AppFilmDTO> fromSwapiFilmListToAppFilmList(List<SwapiFilmDTO> swfilms) {

        return swfilms.stream().map(s -> fromSwapiFilmToAppFilm(s)).collect(Collectors.toList());
    }

    private final AppFilmDTO fromSwapiFilmToAppFilm(SwapiFilmDTO swapiFilmDto) {
        return new AppFilmDTO.Builder().name(swapiFilmDto.getTitle()).releaseDate(swapiFilmDto.getReleaseDate())
                .build();
    }

    public final List<TransportType> convertToAppTransport(List<SwapiVehicleDTO> sVehicleDtos,
            List<SwapiStarshipDTO> sStarshDtos) {

        List<TransportType> transports = fromSwapiVehicleToAppTransportList(sVehicleDtos);
        transports.addAll(fromSwapiStarshipToAppTransportList(sStarshDtos));

        return transports;
    }

    private final List<TransportType> fromSwapiVehicleToAppTransportList(List<SwapiVehicleDTO> sVehicleDtos) {
        return sVehicleDtos.stream().map(s -> fromSwapiVehicleToAppTransport(s)).collect(Collectors.toList());

    }

    private final List<TransportType> fromSwapiStarshipToAppTransportList(List<SwapiStarshipDTO> sStarshDtos) {
        return sStarshDtos.stream().map(s -> fromSwapiStarshipToAppTransport(s)).collect(Collectors.toList());

    }

    private final TransportType fromSwapiVehicleToAppTransport(SwapiVehicleDTO swVehicleDto) {

        String atmSpeedString = swVehicleDto.getMaxAtmospheringSpeed();
        long speedVal = AtmSpeedToLong(atmSpeedString);

        return new TransportType(swVehicleDto.getName(), speedVal);

    }

    private final TransportType fromSwapiStarshipToAppTransport(SwapiStarshipDTO sStarshDto) {

        String atmSpeedString = sStarshDto.getMaxAtmospheringSpeed();
        long speedVal = AtmSpeedToLong(atmSpeedString);

        return new TransportType(sStarshDto.getName(), speedVal);

    }

    /*
     * If "n/a" is given as speed or if the string cannot be converted to long return -1 The api can return in one case
     * a speed of the form "1000km", so we are tryng to parse a second time taking care of this.
     */
    private final long AtmSpeedToLong(String atmSpeedString) {

        long speedVal = NO_SPEED_VALUE;

        try {
            speedVal = Long.valueOf(atmSpeedString);
        } catch (NumberFormatException e1) {
            if (!atmSpeedString.equalsIgnoreCase("n/a")) {
                try {
                    speedVal = Long.valueOf(atmSpeedString.substring(0, atmSpeedString.length() - 2));
                } catch (NumberFormatException e2) {
                    log.error("Cannot parse {} into long", atmSpeedString);
                    return speedVal;
                }
            }
        }

        return speedVal;
    }

}
