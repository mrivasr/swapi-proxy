package com.trileuco.landing.swapiproxy.dtos;

import java.time.LocalDate;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = AppFilmDTO.Builder.class)
public class AppFilmDTO {

    private String name;
    @JsonProperty("release_date")
    private LocalDate releaseDate;

    public AppFilmDTO() {

    }

    public AppFilmDTO(String name, LocalDate releaseDate) {
        this.name = name;
        this.releaseDate = releaseDate;
    }

    public String getName() {
        return name;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder {

        private String name;
        @JsonProperty("release_date")
        private LocalDate releaseDate;

        public Builder() {

        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder releaseDate(LocalDate releaseDate) {
            this.releaseDate = releaseDate;
            return this;
        }

        public AppFilmDTO build() {
            return new AppFilmDTO(name, releaseDate);
        }

    }

}
