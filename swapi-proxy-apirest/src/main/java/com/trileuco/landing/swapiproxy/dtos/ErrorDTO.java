package com.trileuco.landing.swapiproxy.dtos;

public class ErrorDTO {

    private String errorInfo;

    public ErrorDTO(String errorInfo) {
        this.errorInfo = errorInfo;
    }

    public String getErrorInfo() {
        return errorInfo;
    }

    public void setErrorInfo(String errorInfo) {
        this.errorInfo = errorInfo;
    }

}
