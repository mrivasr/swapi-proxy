package com.trileuco.landing.swapiproxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.trileuco.landing.swapiproxy")
public class SwapiproxyApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwapiproxyApplication.class, args);
    }

}
