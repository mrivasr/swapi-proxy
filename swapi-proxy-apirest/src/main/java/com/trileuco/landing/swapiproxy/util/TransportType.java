package com.trileuco.landing.swapiproxy.util;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class TransportType {

    private String name;
    private long atmospheringSpeed;

    public TransportType() {

    }

    public TransportType(String name, long atmospheringSpeed) {
        this.name = name;
        this.atmospheringSpeed = atmospheringSpeed;
    }

    public String getName() {
        return name;
    }

    public long getAtmospheringSpeed() {
        return atmospheringSpeed;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

}
