package com.trileuco.landing.swapiproxy.exceptions;

@SuppressWarnings("serial")
public class SearchNotFoundException extends Exception {

    private String keywords;

    public SearchNotFoundException(String keywords) {
        this.keywords = keywords;
    }

    public String getKeywords() {
        return keywords;
    }

    public String getErrorMessage() {
        return "Keywords '" + getKeywords() + "': No character found";
    }

}
