package com.trileuco.landing.swapiproxy.dtos;

import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = AppPeopleDTO.Builder.class)
public class AppPeopleDTO {

    private String name;

    @JsonProperty("birth_year")
    private String birthYear;

    private String gender;

    @JsonProperty("planet_name")
    private String planetName;

    @JsonProperty("fastest_vehicle_driven")
    private String fastestVehicleDriven;

    private List<AppFilmDTO> films;

    public AppPeopleDTO() {

    }

    public AppPeopleDTO(String name, String birthYear, String gender, String planetName, String fastestVehicleDriven,
            List<AppFilmDTO> films) {

        this.name = name;
        this.birthYear = birthYear;
        this.gender = gender;
        this.planetName = planetName;
        this.fastestVehicleDriven = fastestVehicleDriven;
        this.films = films;
    }

    public String getName() {
        return name;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public String getGender() {
        return gender;
    }

    public String getPlanetName() {
        return planetName;
    }

    public String getFastestVehicleDriven() {
        return fastestVehicleDriven;
    }

    public List<AppFilmDTO> getFilms() {
        return films;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder {

        private String name;

        @JsonProperty("birth_year")
        private String birthYear;

        private String gender;

        @JsonProperty("planet_name")
        private String planetName;

        @JsonProperty("fastest_vehicle_driven")
        private String fastestVehicleDriven;
        private List<AppFilmDTO> films;

        public Builder() {

        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder birthYear(String birthYear) {
            this.birthYear = birthYear;
            return this;
        }

        public Builder gender(String gender) {
            this.gender = gender;
            return this;
        }

        public Builder planetName(String planetName) {
            this.planetName = planetName;
            return this;
        }

        public Builder fastestVehicleDriven(String fastestVehicleDriven) {
            this.fastestVehicleDriven = fastestVehicleDriven;
            return this;
        }

        public Builder films(List<AppFilmDTO> films) {
            this.films = films;
            return this;
        }

        public AppPeopleDTO build() {
            return new AppPeopleDTO(name, birthYear, gender, planetName, fastestVehicleDriven, films);
        }

    }

}
