package com.trileuco.landing.swapiproxy.services;

import java.util.List;

import com.trileuco.landing.swapiproxy.dtos.AppPeopleDTO;
import com.trileuco.landing.swapiproxy.exceptions.SearchNotFoundException;

public interface SWapiService {

    public List<AppPeopleDTO> getCharacteresByName(String searchUrl, String name) throws SearchNotFoundException;

}
