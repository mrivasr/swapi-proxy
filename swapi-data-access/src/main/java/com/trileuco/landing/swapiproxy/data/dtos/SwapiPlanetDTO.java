package com.trileuco.landing.swapiproxy.data.dtos;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = SwapiPlanetDTO.Builder.class)
public class SwapiPlanetDTO {

    private String climate;
    private LocalDateTime created;
    private String diameter;
    private LocalDateTime edited;
    private List<String> films;
    private String gravity;
    private String name;

    @JsonProperty("orbital_period")
    private String orbitalPeriod;

    private List<String> residents;

    @JsonProperty("rotation_period")
    private String rotationPeriod;

    @JsonProperty("surface_water")
    private String surfaceWater;

    private String population;
    private String terrain;
    private String url;

    public SwapiPlanetDTO() {
    }

    public SwapiPlanetDTO(String climate, LocalDateTime created, String diameter, LocalDateTime edited,
            List<String> films, String gravity, String name, String orbitalPeriod, List<String> residents,
            String rotationPeriod, String surfaceWater, String population, String terrain, String url) {

        this.climate = climate;
        this.created = created;
        this.diameter = diameter;
        this.edited = edited;
        this.films = films;
        this.gravity = gravity;
        this.name = name;
        this.orbitalPeriod = orbitalPeriod;
        this.residents = residents;
        this.rotationPeriod = rotationPeriod;
        this.surfaceWater = surfaceWater;
        this.population = population;
        this.terrain = terrain;
        this.url = url;
    }

    public String getClimate() {
        return climate;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public String getDiameter() {
        return diameter;
    }

    public LocalDateTime getEdited() {
        return edited;
    }

    public List<String> getFilms() {
        return films;
    }

    public String getGravity() {
        return gravity;
    }

    public String getName() {
        return name;
    }

    public String getOrbitalPeriod() {
        return orbitalPeriod;
    }

    public List<String> getResidents() {
        return residents;
    }

    public String getRotationPeriod() {
        return rotationPeriod;
    }

    public String getSurfaceWater() {
        return surfaceWater;
    }

    public String getPopulation() {
        return population;
    }

    public String getTerrain() {
        return terrain;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder {

        private String climate;
        private LocalDateTime created;
        private String diameter;
        private LocalDateTime edited;
        private List<String> films;
        private String gravity;
        private String name;

        @JsonProperty("orbital_period")
        private String orbitalPeriod;

        private List<String> residents;

        @JsonProperty("rotation_period")
        private String rotationPeriod;

        @JsonProperty("surface_water")
        private String surfaceWater;

        private String population;
        private String terrain;
        private String url;

        public Builder climate(String climate) {
            this.climate = climate;
            return this;
        }

        public Builder created(LocalDateTime created) {
            this.created = created;
            return this;
        }

        public Builder diameter(String diameter) {
            this.diameter = diameter;
            return this;
        }

        public Builder edited(LocalDateTime edited) {
            this.edited = edited;
            return this;
        }

        public Builder films(List<String> films) {
            this.films = films;
            return this;
        }

        public Builder gravity(String gravity) {
            this.gravity = gravity;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder orbitalPeriod(String orbitalPeriod) {
            this.orbitalPeriod = orbitalPeriod;
            return this;
        }

        public Builder residents(List<String> residents) {
            this.residents = residents;
            return this;
        }

        public Builder rotationPeriod(String rotationPeriod) {
            this.rotationPeriod = rotationPeriod;
            return this;
        }

        public Builder surfaceWater(String surfaceWater) {
            this.surfaceWater = surfaceWater;
            return this;
        }

        public Builder population(String population) {
            this.population = population;
            return this;
        }

        public Builder terrain(String terrain) {
            this.terrain = terrain;
            return this;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public SwapiPlanetDTO build() {
            return new SwapiPlanetDTO(climate, created, diameter, edited, films, gravity, name, orbitalPeriod,
                    residents, rotationPeriod, surfaceWater, population, terrain, url);
        }

    }

}
