package com.trileuco.landing.swapiproxy.data.dataservices;

import org.springframework.stereotype.Service;

import com.trileuco.landing.swapiproxy.data.dtos.SwapiSearchResultDTO;

@Service
public class SearchService extends SwapiDataService {

    public SwapiSearchResultDTO getCharacteresByKeywordsFromUrl(String url) {
        return urlToDto(url, SwapiSearchResultDTO.class);
    }

}
