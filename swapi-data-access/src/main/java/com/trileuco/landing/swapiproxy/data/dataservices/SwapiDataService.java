package com.trileuco.landing.swapiproxy.data.dataservices;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.client.RestTemplate;

public abstract class SwapiDataService {

    public final <T> List<T> urlListToDtos(List<String> urls, Class<T> cType) {
        return urls.stream().map(u -> urlToDto(u, cType)).collect(Collectors.toList());
    }

    public final <T> T urlToDto(String url, Class<T> cType) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(url, cType);
    }

}
