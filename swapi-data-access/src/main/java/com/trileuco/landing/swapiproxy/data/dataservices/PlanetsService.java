package com.trileuco.landing.swapiproxy.data.dataservices;

import java.util.List;

import org.springframework.stereotype.Service;

import com.trileuco.landing.swapiproxy.data.dtos.SwapiPlanetDTO;

@Service
public class PlanetsService extends SwapiDataService {

    public List<SwapiPlanetDTO> getSwapiPlanetsFromUrlList(List<String> listUrls) {
        return urlListToDtos(listUrls, SwapiPlanetDTO.class);
    }

    public SwapiPlanetDTO getSwapiPlanetFromUrl(String url) {
        return urlToDto(url, SwapiPlanetDTO.class);
    }

}
