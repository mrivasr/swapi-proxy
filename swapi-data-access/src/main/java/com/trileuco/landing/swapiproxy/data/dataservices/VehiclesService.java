package com.trileuco.landing.swapiproxy.data.dataservices;

import java.util.List;

import org.springframework.stereotype.Service;

import com.trileuco.landing.swapiproxy.data.dtos.SwapiVehicleDTO;

@Service
public class VehiclesService extends SwapiDataService {

    public List<SwapiVehicleDTO> getSwapiVehiclesFromUrlList(List<String> listUrls) {
        return urlListToDtos(listUrls, SwapiVehicleDTO.class);
    }

    public SwapiVehicleDTO getSwapiVehicleFromUrl(String url) {
        return urlToDto(url, SwapiVehicleDTO.class);
    }

}