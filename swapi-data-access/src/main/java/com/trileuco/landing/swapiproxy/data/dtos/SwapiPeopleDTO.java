package com.trileuco.landing.swapiproxy.data.dtos;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = SwapiPeopleDTO.Builder.class)
public class SwapiPeopleDTO {

    private String name;
    private String height;
    private String mass;

    @JsonProperty("hair_color")
    private String hairColor;

    @JsonProperty("skin_color")
    private String skinColor;

    @JsonProperty("eye_color")
    private String eyeColor;

    @JsonProperty("birth_year")
    private String birthYear;

    private String gender;
    private String homeworld;
    private List<String> films;
    private List<String> species;
    private List<String> vehicles;
    private List<String> starships;
    private LocalDateTime created;
    private LocalDateTime edited;
    private String url;

    public SwapiPeopleDTO() {
    }

    public SwapiPeopleDTO(String name, String height, String mass, String hairColor, String skinColor, String eyeColor,
            String birthYear, String gender, String homeworld, List<String> films, List<String> species,
            List<String> vehicles, List<String> starships, LocalDateTime created, LocalDateTime edited, String url) {

        this.name = name;
        this.height = height;
        this.mass = mass;
        this.hairColor = hairColor;
        this.skinColor = skinColor;
        this.eyeColor = eyeColor;
        this.birthYear = birthYear;
        this.gender = gender;
        this.homeworld = homeworld;
        this.films = films;
        this.species = species;
        this.vehicles = vehicles;
        this.starships = starships;
        this.created = created;
        this.edited = edited;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getHeight() {
        return height;
    }

    public String getMass() {
        return mass;
    }

    public String getHairColor() {
        return hairColor;
    }

    public String getSkinColor() {
        return skinColor;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public String getGender() {
        return gender;
    }

    public String getHomeworld() {
        return homeworld;
    }

    public List<String> getFilms() {
        return films;
    }

    public List<String> getSpecies() {
        return species;
    }

    public List<String> getVehicles() {
        return vehicles;
    }

    public List<String> getStarships() {
        return starships;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getEdited() {
        return edited;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder {

        private String name;
        private String height;
        private String mass;

        @JsonProperty("hair_color")
        private String hairColor;

        @JsonProperty("skin_color")
        private String skinColor;

        @JsonProperty("eye_color")
        private String eyeColor;

        @JsonProperty("birth_year")
        private String birthYear;

        private String gender;
        private String homeworld;
        private List<String> films;
        private List<String> species;
        private List<String> vehicles;
        private List<String> starships;
        private LocalDateTime created;
        private LocalDateTime edited;
        private String url;

        public Builder() {
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder height(String height) {
            this.height = height;
            return this;
        }

        public Builder mass(String mass) {
            this.mass = mass;
            return this;
        }

        public Builder hairColor(String hairColor) {
            this.hairColor = hairColor;
            return this;
        }

        public Builder skinColor(String skinColor) {
            this.skinColor = skinColor;
            return this;
        }

        public Builder eyeColor(String eyeColor) {
            this.eyeColor = eyeColor;
            return this;
        }

        public Builder birthYear(String birthYear) {
            this.birthYear = birthYear;
            return this;
        }

        public Builder gender(String gender) {
            this.gender = gender;
            return this;
        }

        public Builder homeworld(String homeworld) {
            this.homeworld = homeworld;
            return this;
        }

        public Builder films(List<String> films) {
            this.films = films;
            return this;
        }

        public Builder species(List<String> species) {
            this.species = species;
            return this;
        }

        public Builder vehicles(List<String> vehicles) {
            this.vehicles = vehicles;
            return this;
        }

        public Builder starships(List<String> starships) {
            this.starships = starships;
            return this;
        }

        public Builder created(LocalDateTime created) {
            this.created = created;
            return this;
        }

        public Builder edited(LocalDateTime edited) {
            this.edited = edited;
            return this;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public SwapiPeopleDTO build() {
            return new SwapiPeopleDTO(name, height, mass, hairColor, skinColor, eyeColor, birthYear, gender, homeworld,
                    films, species, vehicles, starships, created, edited, url);
        }

    }

}
