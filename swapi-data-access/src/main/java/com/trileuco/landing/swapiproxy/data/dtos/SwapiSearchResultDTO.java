package com.trileuco.landing.swapiproxy.data.dtos;

import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = SwapiSearchResultDTO.Builder.class)
public class SwapiSearchResultDTO {

    private int count;
    private String next;
    private String preious;
    private List<SwapiPeopleDTO> results;

    public SwapiSearchResultDTO() {
    }

    public SwapiSearchResultDTO(int count, String next, String preious, List<SwapiPeopleDTO> results) {
        this.count = count;
        this.next = next;
        this.preious = preious;
        this.results = results;
    }

    public int getCount() {
        return count;
    }

    public String getNext() {
        return next;
    }

    public String getPreious() {
        return preious;
    }

    public List<SwapiPeopleDTO> getResults() {
        return results;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder {

        private int count;
        private String next;
        private String preious;
        private List<SwapiPeopleDTO> results;

        public Builder() {

        }

        public Builder count(int count) {
            this.count = count;
            return this;
        }

        public Builder next(String next) {
            this.next = next;
            return this;
        }

        public Builder preious(String preious) {
            this.preious = preious;
            return this;
        }

        public Builder results(List<SwapiPeopleDTO> results) {
            this.results = results;
            return this;
        }

        public SwapiSearchResultDTO build() {
            return new SwapiSearchResultDTO(count, next, preious, results);
        }

    }

}
