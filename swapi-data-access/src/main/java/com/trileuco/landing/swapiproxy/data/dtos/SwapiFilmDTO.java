package com.trileuco.landing.swapiproxy.data.dtos;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = SwapiFilmDTO.Builder.class)
public class SwapiFilmDTO {

    private List<String> characters;
    private LocalDateTime created;
    private String director;
    private LocalDateTime edited;

    @JsonProperty("episode_id")
    private int episodeId;

    @JsonProperty("opening_crawl")
    private String openingCrawl;

    private List<String> planets;
    private String producer;

    @JsonProperty("release_date")
    private LocalDate releaseDate;

    private List<String> species;
    private List<String> starships;
    private String title;
    private String url;
    private List<String> vehicles;

    public SwapiFilmDTO() {

    }

    public SwapiFilmDTO(List<String> characters, LocalDateTime created, String director, LocalDateTime edited,
            int episodeId, String openingCrawl, List<String> planets, String producer, LocalDate releaseDate,
            List<String> species, List<String> starships, String title, String url, List<String> vehicles) {

        this.characters = characters;
        this.created = created;
        this.director = director;
        this.edited = edited;
        this.episodeId = episodeId;
        this.openingCrawl = openingCrawl;
        this.planets = planets;
        this.producer = producer;
        this.releaseDate = releaseDate;
        this.species = species;
        this.starships = starships;
        this.title = title;
        this.url = url;
        this.vehicles = vehicles;
    }

    public List<String> getCharacters() {
        return characters;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public String getDirector() {
        return director;
    }

    public LocalDateTime getEdited() {
        return edited;
    }

    public int getEpisodeId() {
        return episodeId;
    }

    public String getOpeningCrawl() {
        return openingCrawl;
    }

    public List<String> getPlanets() {
        return planets;
    }

    public String getProducer() {
        return producer;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public List<String> getSpecies() {
        return species;
    }

    public List<String> getStarships() {
        return starships;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public List<String> getVehicles() {
        return vehicles;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder {

        private List<String> characters;
        private LocalDateTime created;
        private String director;
        private LocalDateTime edited;

        @JsonProperty("episode_id")
        private int episodeId;

        @JsonProperty("opening_crawl")
        private String openingCrawl;

        private List<String> planets;
        private String producer;

        @JsonProperty("release_date")
        private LocalDate releaseDate;

        private List<String> species;
        private List<String> starships;
        private String title;
        private String url;
        private List<String> vehicles;

        public Builder() {
        }

        public Builder characters(List<String> characters) {
            this.characters = characters;
            return this;
        }

        public Builder created(LocalDateTime created) {
            this.created = created;
            return this;
        }

        public Builder director(String director) {
            this.director = director;
            return this;
        }

        public Builder edited(LocalDateTime edited) {
            this.edited = edited;
            return this;
        }

        public Builder episodeId(int episodeId) {
            this.episodeId = episodeId;
            return this;
        }

        public Builder openingCrawl(String openingCrawl) {
            this.openingCrawl = openingCrawl;
            return this;
        }

        public Builder planets(List<String> planets) {
            this.planets = planets;
            return this;
        }

        public Builder producer(String producer) {
            this.producer = producer;
            return this;
        }

        public Builder releaseDate(LocalDate releaseDate) {
            this.releaseDate = releaseDate;
            return this;
        }

        public Builder species(List<String> species) {
            this.species = species;
            return this;
        }

        public Builder starships(List<String> starships) {
            this.starships = starships;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public Builder vehicles(List<String> vehicles) {
            this.vehicles = vehicles;
            return this;
        }

        public SwapiFilmDTO build() {
            return new SwapiFilmDTO(characters, created, director, edited, episodeId, openingCrawl, planets, producer,
                    releaseDate, species, starships, title, url, vehicles);
        }
    }

}
