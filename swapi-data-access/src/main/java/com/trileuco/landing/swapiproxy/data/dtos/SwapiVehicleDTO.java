package com.trileuco.landing.swapiproxy.data.dtos;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = SwapiVehicleDTO.Builder.class)
public class SwapiVehicleDTO {

    @JsonProperty("cargo_capacity")
    private String cargoCapacity;

    private String consumables;

    @JsonProperty("cost_in_credits")
    private String costInCredits;

    private LocalDateTime created;
    private String crew;
    private LocalDateTime edited;
    private String length;
    private String manufacturer;

    @JsonProperty("max_atmosphering_speed")
    private String maxAtmospheringSpeed; // possible values like "n/a" or numeric value

    private String model;
    private String name;
    private String passengers;
    private List<String> pilots;
    private List<String> films;
    private String url;

    @JsonProperty("vehicle_class")
    private String vehicleClass;

    public SwapiVehicleDTO() {

    }

    public SwapiVehicleDTO(String cargoCapacity, String consumables, String costInCredits, LocalDateTime created,
            String crew, LocalDateTime edited, String length, String manufacturer, String maxAtmospheringSpeed,
            String model, String name, String passengers, List<String> pilots, List<String> films, String url,
            String vehicleClass) {

        this.cargoCapacity = cargoCapacity;
        this.consumables = consumables;
        this.costInCredits = costInCredits;
        this.created = created;
        this.crew = crew;
        this.edited = edited;
        this.length = length;
        this.manufacturer = manufacturer;
        this.maxAtmospheringSpeed = maxAtmospheringSpeed;
        this.model = model;
        this.name = name;
        this.passengers = passengers;
        this.pilots = pilots;
        this.films = films;
        this.url = url;
        this.vehicleClass = vehicleClass;
    }

    public String getCargoCapacity() {
        return cargoCapacity;
    }

    public String getConsumables() {
        return consumables;
    }

    public String getCostInCredits() {
        return costInCredits;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public String getCrew() {
        return crew;
    }

    public LocalDateTime getEdited() {
        return edited;
    }

    public String getLength() {
        return length;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getMaxAtmospheringSpeed() {
        return maxAtmospheringSpeed;
    }

    public String getModel() {
        return model;
    }

    public String getName() {
        return name;
    }

    public String getPassengers() {
        return passengers;
    }

    public List<String> getPilots() {
        return pilots;
    }

    public List<String> getFilms() {
        return films;
    }

    public String getUrl() {
        return url;
    }

    public String getVehicleClass() {
        return vehicleClass;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder {

        @JsonProperty("cargo_capacity")
        private String cargoCapacity;

        private String consumables;

        @JsonProperty("cost_in_credits")
        private String costInCredits;

        private LocalDateTime created;
        private String crew;
        private LocalDateTime edited;
        private String length;
        private String manufacturer;

        @JsonProperty("max_atmosphering_speed")
        private String maxAtmospheringSpeed;

        private String model;
        private String name;
        private String passengers;
        private List<String> pilots;
        private List<String> films;
        private String url;

        @JsonProperty("vehicle_class")
        private String vehicleClass;

        public Builder() {

        }

        public Builder cargoCapacity(String cargoCapacity) {
            this.cargoCapacity = cargoCapacity;
            return this;
        }

        public Builder consumables(String consumables) {
            this.consumables = consumables;
            return this;
        }

        public Builder costInCredits(String costInCredits) {
            this.costInCredits = costInCredits;
            return this;
        }

        public Builder created(LocalDateTime created) {
            this.created = created;
            return this;
        }

        public Builder crew(String crew) {
            this.crew = crew;
            return this;
        }

        public Builder edited(LocalDateTime edited) {
            this.edited = edited;
            return this;
        }

        public Builder length(String length) {
            this.length = length;
            return this;
        }

        public Builder manufacturer(String manufacturer) {
            this.manufacturer = manufacturer;
            return this;
        }

        public Builder maxAtmospheringSpeed(String maxAtmospheringSpeed) {
            this.maxAtmospheringSpeed = maxAtmospheringSpeed;
            return this;
        }

        public Builder model(String model) {
            this.model = model;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder passengers(String passengers) {
            this.passengers = passengers;
            return this;
        }

        public Builder pilots(List<String> pilots) {
            this.pilots = pilots;
            return this;
        }

        public Builder films(List<String> films) {
            this.films = films;
            return this;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public Builder vehicleClass(String vehicleClass) {
            this.vehicleClass = vehicleClass;
            return this;
        }

        public SwapiVehicleDTO build() {
            return new SwapiVehicleDTO(cargoCapacity, consumables, costInCredits, created, crew, edited, length,
                    manufacturer, maxAtmospheringSpeed, model, name, passengers, pilots, films, url, vehicleClass);
        }

    }

}
