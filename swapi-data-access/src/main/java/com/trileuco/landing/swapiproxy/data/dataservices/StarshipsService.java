package com.trileuco.landing.swapiproxy.data.dataservices;

import java.util.List;

import org.springframework.stereotype.Service;

import com.trileuco.landing.swapiproxy.data.dtos.SwapiStarshipDTO;

@Service
public class StarshipsService extends SwapiDataService {

    public List<SwapiStarshipDTO> getSwapiStarshipsFromUrlList(List<String> listUrls) {
        return urlListToDtos(listUrls, SwapiStarshipDTO.class);
    }

    public SwapiStarshipDTO getSwpiStarshipFromUrl(String url) {
        return urlToDto(url, SwapiStarshipDTO.class);
    }

}
