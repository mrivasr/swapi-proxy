package com.trileuco.landing.swapiproxy.data.dataservices;

import java.util.List;

import org.springframework.stereotype.Service;

import com.trileuco.landing.swapiproxy.data.dtos.SwapiFilmDTO;

@Service
public class FilmsService extends SwapiDataService {

    public List<SwapiFilmDTO> getSwapiFilmsFromUrlLists(List<String> listUrls) {
        return urlListToDtos(listUrls, SwapiFilmDTO.class);
    }

    public SwapiFilmDTO getSwapiFilmFromUrl(String url) {
        return urlToDto(url, SwapiFilmDTO.class);
    }

}
