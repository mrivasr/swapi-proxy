package com.trileuco.landing.swapiproxy.data.dtos;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = SwapiStarshipDTO.Builder.class)
public class SwapiStarshipDTO {

    private String mglt;

    @JsonProperty("cargo_capacity")
    private String cargoCapacity;

    private String consumables;

    @JsonProperty("cost_in_credits")
    private String costInCredits;

    private LocalDateTime created;
    private String crew;
    private LocalDateTime edited;

    @JsonProperty("hyperdrive_rating")
    private String hyperdriveRating;

    private String length;
    private String manufacturer;

    @JsonProperty("max_atmosphering_speed")
    private String maxAtmospheringSpeed;

    private String model;
    private String name;
    private String passengers;
    private List<String> films;
    private List<String> pilots;

    @JsonProperty("starship_class")
    private String starshipClass;

    private String url;

    public SwapiStarshipDTO() {

    }

    public SwapiStarshipDTO(String mglt, String cargoCapacity, String consumables, String costInCredits,
            LocalDateTime created, String crew, LocalDateTime edited, String hyperdriveRating, String length,
            String manufacturer, String maxAtmospheringSpeed, String model, String name, String passengers,
            List<String> films, List<String> pilots, String starshipClass, String url) {

        this.mglt = mglt;
        this.cargoCapacity = cargoCapacity;
        this.consumables = consumables;
        this.costInCredits = costInCredits;
        this.created = created;
        this.crew = crew;
        this.edited = edited;
        this.hyperdriveRating = hyperdriveRating;
        this.length = length;
        this.manufacturer = manufacturer;
        this.maxAtmospheringSpeed = maxAtmospheringSpeed;
        this.model = model;
        this.name = name;
        this.passengers = passengers;
        this.films = films;
        this.pilots = pilots;
        this.starshipClass = starshipClass;
        this.url = url;
    }

    public String getMglt() {
        return mglt;
    }

    public String getCargoCapacity() {
        return cargoCapacity;
    }

    public String getConsumables() {
        return consumables;
    }

    public String getCostInCredits() {
        return costInCredits;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public String getCrew() {
        return crew;
    }

    public LocalDateTime getEdited() {
        return edited;
    }

    public String getHyperdriveRating() {
        return hyperdriveRating;
    }

    public String getLength() {
        return length;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getMaxAtmospheringSpeed() {
        return maxAtmospheringSpeed;
    }

    public String getModel() {
        return model;
    }

    public String getName() {
        return name;
    }

    public String getPassengers() {
        return passengers;
    }

    public List<String> getFilms() {
        return films;
    }

    public List<String> getPilots() {
        return pilots;
    }

    public String getStarshipClass() {
        return starshipClass;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder {

        private String mglt;

        @JsonProperty("cargo_capacity")
        private String cargoCapacity;

        private String consumables;

        @JsonProperty("cost_in_credits")
        private String costInCredits;

        private LocalDateTime created;
        private String crew;
        private LocalDateTime edited;

        @JsonProperty("hyperdrive_rating")
        private String hyperdriveRating;

        private String length;
        private String manufacturer;

        @JsonProperty("max_atmosphering_speed")
        private String maxAtmospheringSpeed;

        private String model;
        private String name;
        private String passengers;
        private List<String> films;
        private List<String> pilots;

        @JsonProperty("starship_class")
        private String starshipClass;

        private String url;

        public Builder() {
        }

        public Builder mglt(String mglt) {
            this.mglt = mglt;
            return this;
        }

        public Builder cargo_capacity(String cargoCapacity) {
            this.cargoCapacity = cargoCapacity;
            return this;
        }

        public Builder consumables(String consumables) {
            this.consumables = consumables;
            return this;
        }

        public Builder cost_in_credits(String costInCredits) {
            this.costInCredits = costInCredits;
            return this;
        }

        public Builder created(LocalDateTime created) {
            this.created = created;
            return this;
        }

        public Builder crew(String crew) {
            this.crew = crew;
            return this;
        }

        public Builder edited(LocalDateTime edited) {
            this.edited = edited;
            return this;
        }

        public Builder hyperdrive_rating(String hyperdriveRating) {
            this.hyperdriveRating = hyperdriveRating;
            return this;
        }

        public Builder length(String length) {
            this.length = length;
            return this;
        }

        public Builder manufacturer(String manufacturer) {
            this.manufacturer = manufacturer;
            return this;
        }

        public Builder max_atmosphering_speed(String maxAtmospheringSpeed) {
            this.maxAtmospheringSpeed = maxAtmospheringSpeed;
            return this;
        }

        public Builder model(String model) {
            this.model = model;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder passengers(String passengers) {
            this.passengers = passengers;
            return this;
        }

        public Builder films(List<String> films) {
            this.films = films;
            return this;
        }

        public Builder pilots(List<String> pilots) {
            this.pilots = pilots;
            return this;
        }

        public Builder starship_class(String starshipClass) {
            this.starshipClass = starshipClass;
            return this;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public SwapiStarshipDTO build() {
            return new SwapiStarshipDTO(mglt, cargoCapacity, consumables, costInCredits, created, crew, edited,
                    hyperdriveRating, length, manufacturer, maxAtmospheringSpeed, model, name, passengers, films,
                    pilots, starshipClass, url);
        }

    }

}
