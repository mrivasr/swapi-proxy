# Swapi Proxy
Landing Weeks exercise. 
RESTFul API based in Spring Boot. 
This API will allow to get information about Star Wars characters.

Since 4/03/2020 the application is divided in 2 modules:

- **swapi-data-access**: Implements a series of repositories that can retrieve data from swapi.co given an access URL, and POJOs that encapsulates all the returned Json fields.

- **swapi-proxy-apirest**: Implements the api rest controllers and its logic behind them. This module will be the executable of the application. 


## Requirements
- Java SE 8+
- Maven 3+
- *Docker (optional)*

## Run
In root directory:

```
mvn install
java -jar swapi-proxy-apirest/target/swapi-proxy-apirest-0.0.1-SNAPSHOT.jar
```


Or you can just execute Maven spring boot plugin (useful in development)

```
mvn spring-boot:run
```

## Usage
The application accepts petitions of the type:

```
http://localhost:8080/swapi-proxy/person-info?name=XYZ
```
Where XYZ are the keywords to find characteres by name.

## Dockerization
As a little exercise about docker usage and integration, this exercise implements maven fabric8 plugin to manage docker builds and automatic publication. It supports image building and publishing to dockerhub repo, so in order to get the latest build of the app just pull the image from:
<https://hub.docker.com/r/mrivas13/swapiproxy> 

```
docker pull mrivas13/swapiproxy:latest
```

And then just run the container:

```
docker run -d -p 8080:8080 mrivas13/swapiproxy
```



---
### Fabric8 plugin usage
Since the application is divided in modules this plugin is embedded in the swapi-proxy-apirest, for this is the executable module. In order to use the plugin you must call it from the ***swapi-proxy-apirest*** directory.


To build a docker image locally:

```
mvn clean package fabric8:build
```

You can now run Swapi proxy from a docker container:

```
docker run -d -p 8080:8080 mrivas13/swapiproxy
```
---
To build and publish a docker image to a repository (credentials in maven settings.xml needed)

```
mvn clean package fabric8:build fabric8:push
```

And then run the app using *docker run cmd*.